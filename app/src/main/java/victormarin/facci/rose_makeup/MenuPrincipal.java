package victormarin.facci.rose_makeup;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MenuPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionCatalogo:
                intent = new Intent(MenuPrincipal.this, Catalogo.class);
                startActivity(intent);
                break;
            case R.id.opcionCitasReservadas:
                intent = new Intent(MenuPrincipal.this, ListaReservaciones.class);
                startActivity(intent);
                break;
            case R.id.opcionReservacion:
                intent = new Intent(MenuPrincipal.this, Reservacion.class);
                startActivity(intent);
                break;
        }
        return true;
    }
}

